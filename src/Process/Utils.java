/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Process;

import com.toedter.calendar.JDateChooser;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

/**
 *
 * @author DAT
 */
public class Utils {

    public static void exitOptionDialog(Frame frame) {
        String[] options = {"No", "Yes"};
        int x = JOptionPane.showOptionDialog(null, "Are you want to exit?", "Quit", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (x == 1) {
            System.exit(0);
        }
//    int answer = JOptionPane.showConfirmDialog(this, "Are you want to exit?", "Quit", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, JOptionPane);
//    if (answer == JOptionPane.YES_OPTION){
//        System.exit(0);
//    }
    }

    public static void preventClosingWindowFromCrossButton(Frame frame) {
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Utils.exitOptionDialog(frame);
            }
        });
    }

    public static void setDate(String txtDate, JDateChooser dateChooser) throws ParseException {
        Date date;
        date = (Date) new SimpleDateFormat("dd-MM-yyyy").parse(txtDate);
        dateChooser.setDate(date);
    }

    public static String getDate(JDateChooser dateChooser) {
        Date date = (Date) dateChooser.getDate();
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(date);
    }

    public static String getDateSql(JDateChooser dateChooser) {
        Date date = (Date) dateChooser.getDate();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    public static void shiftDate(JDateChooser oldD,JDateChooser newD, int diff) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String oldDate = df.format(oldD.getDate());
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, diff);

        String newDate = df.format(c.getTime());
        Date Edate = null;
        try {
            Edate = df.parse(newDate);
            newD.setDate(Edate);

        } catch (ParseException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

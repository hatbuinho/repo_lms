CREATE DEFINER=`root`@`localhost` PROCEDURE `create_lendingbook`(
	IN umember_id int(11),
	IN ubookitem_id int(11),
	IN udue_date date,
	IN ucreate_date date
)
BEGIN
	DECLARE uid INT DEFAULT 0;
	SET uid = (SELECT id FROM account WHERE account.id = umember_id);
	INSERT INTO book_lending (member_id, book_item_id, due_time, creation_date) VALUES (uid, ubookitem_id, udue_date, ucreate_date);
END